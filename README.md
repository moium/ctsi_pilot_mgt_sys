# CTSI_Pilot_Mgt_Sys

CTSI Pilot Management System

PLEASE NOTE: These files are specific to our implementation, there are many customizations added from the Survey Settings pages that are not supplied from either the Data Dictionaries or the .xml files (this is a REDCap issue). 

CTSI of Southeast Wisconsin is happy to share details including our REDCap data dictionary with other CTSA institutions. Please contact:

o Christine Zeller, CTSI Pilot Award Program Manager, czeller@mcw.edu

o Mark Oium, REDCap Administrator, moium@mcw.edu

**If you plan on implementing** any innovations developed by us into your system, we would appreciate that you acknowledge our role [CTSI of Southeast Wisconsin] when reporting on this as an efficiency measure.

TO DOWNLOAD
Click the Downloads link to the left, then click the 'Download repository' link.

**IMPORTANT**
This repository contains a custom_hooks.php file (utilizing Andy Martins Hook Framework) that has the ability to attach a file to an email being sent to reviewers. In order to make this happen, a slight change to REDCap's base code is needed (must be done for every REDCap upgrade). The code below should replace the email() function located in /redcap/redcap_versiondirectory/Classes/REDCap.php. The only additions to the original email() function is the $attachment_path='' in the parameter list, and the $email->setAttachment($attachment_path); in the list of statements.'

    public static function email($to='', $from='', $subject='', $message='', $cc='', $attachment_path='')
 	{
 		    $email = new Message();
 		    $email->setTo($to);
 		    if ($cc != '') $email->setCc($cc);
 		    $email->setFrom($from);
 		    $email->setSubject($subject);
 		    $email->setBody($message);
            $email->setAttachment($attachment_path);
 		    return $email->send();
 	}