<?php

/*
    Project: CTSI Pilot Management System	
    project_id = 529 
*/

/////////////////////////////////////
/////  REDCAP_SURVEY_PAGE_TOP  //////
/////////////////////////////////////
if ($hook_event == "redcap_survey_page_top" ) {
  ?>
    <style>
        <?php
            // Hide the left TD (used for auto-numbering) if auto-numbering is turned off
            global $question_auto_numbering;
            if ($question_auto_numbering == 0) print "td.questionnum, td.questionnummatrix { display:none !important; }";
        ?>
    </style>
    <script>
        $(document).ready(function() {
            // Place holder for custom js you might want to add...
        });
    </script>
  <?php
}

//////////////////////////////////////
//////  REDCAP_DATA_ENTRY_FORM  //////
//////////////////////////////////////
if ($hook_event == 'redcap_data_entry_form') {

  if ($instrument == 'application_admin') {
//    $fields = REDCap:: getFieldNames($instrument);
//    $data = json_decode(REDCap:: getData('json', $record, $fields, $event_id), true);
//    $email_sent = array();
//    $selected_reviewers = array();
//    for ($i = 1; $i <= 3; $i++) {
//      // populate $selected_reviewers array first
//      $selected_reviewers[] = $data[0]['aa_rev'.$i.'_select'];
//    }
//    $num_prop_data = json_decode(REDCap:: getData(967, 'json', $selected_reviewers, 'num_prop'), true);

    // use JS to gather and populate reviewer fields
    ?>
      <script type='text/javascript'>
          $(document).ready(function () {

              // Reviewer 1        
              $("select[name='aa_rev1_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r1_name = opt_array[0].split(', ');
                      var r1_fname = r1_name[1];
                      var r1_lname = r1_name[0];
                      var r1_email = opt_array[1];

                      $("input[name='aa_rev1_fname']").val(r1_fname);
                      $("input[name='aa_rev1_lname']").val(r1_lname);
                      $("input[name='aa_rev1_email']").val(r1_email);
                  }
              });

              // Reviewer 2
              $("select[name='aa_rev2_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r2_name = opt_array[0].split(', ');
                      var r2_fname = r2_name[1];
                      var r2_lname = r2_name[0];
                      var r2_email = opt_array[1];

                      $("input[name='aa_rev2_fname']").val(r2_fname);
                      $("input[name='aa_rev2_lname']").val(r2_lname);
                      $("input[name='aa_rev2_email']").val(r2_email);
                  }
              });

              // Reviewer 3
              $("select[name='aa_rev3_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r3_name = opt_array[0].split(', ');
                      var r3_fname = r3_name[1];
                      var r3_lname = r3_name[0];
                      var r3_email = opt_array[1];

                      $("input[name='aa_rev3_fname']").val(r3_fname);
                      $("input[name='aa_rev3_lname']").val(r3_lname);
                      $("input[name='aa_rev3_email']").val(r3_email);
                  }
              });

              // Reviewer 4
              $("select[name='aa_rev4_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on |
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r4_name = opt_array[0].split(', ');
                      var r4_fname = r4_name[1];
                      var r4_lname = r4_name[0];
                      var r4_email = opt_array[1];

                      $("input[name='aa_rev4_fname']").val(r4_fname);
                      $("input[name='aa_rev4_lname']").val(r4_lname);
                      $("input[name='aa_rev4_email']").val(r4_email);
                  }
              });


          });
      </script>

    <?php
  }

  //   !! IMPORTANT !!
  // Current instrument name = admin_reviewer_request, but because REDCap
  // keeps the original I have to use the original.
  // If used elsewhere, change this to admin_reviewer_request
  if ($instrument == 'reviewer_request') {
    //print "inst: $instrument";
    // use JS to gather and populate reviewer fields
    ?>
      <script type='text/javascript'>
          $(document).ready(function () {

              // Reviewer 1        
              $("select[name='arr_rev1_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();

                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r1_name = opt_array[0].split(', ');
                      var r1_fname = r1_name[1];
                      var r1_lname = r1_name[0];
                      var r1_email = opt_array[1];

                      $("input[name='arr_rev1_fname']").val(r1_fname);
                      $("input[name='arr_rev1_lname']").val(r1_lname);
                      $("input[name='arr_rev1_email']").val(r1_email);
                  }
              });

              // Reviewer 2
              $("select[name='arr_rev2_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r2_name = opt_array[0].split(', ');
                      var r2_fname = r2_name[1];
                      var r2_lname = r2_name[0];
                      var r2_email = opt_array[1];

                      $("input[name='arr_rev2_fname']").val(r2_fname);
                      $("input[name='arr_rev2_lname']").val(r2_lname);
                      $("input[name='arr_rev2_email']").val(r2_email);
                  }
              });

              // Reviewer 3
              $("select[name='arr_rev3_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r3_name = opt_array[0].split(', ');
                      var r3_fname = r3_name[1];
                      var r3_lname = r3_name[0];
                      var r3_email = opt_array[1];

                      $("input[name='arr_rev3_fname']").val(r3_fname);
                      $("input[name='arr_rev3_lname']").val(r3_lname);
                      $("input[name='arr_rev3_email']").val(r3_email);
                  }
              });

              // Reviewer 4
              $("select[name='arr_rev4_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r4_name = opt_array[0].split(', ');
                      var r4_fname = r4_name[1];
                      var r4_lname = r4_name[0];
                      var r4_email = opt_array[1];

                      $("input[name='arr_rev4_fname']").val(r4_fname);
                      $("input[name='arr_rev4_lname']").val(r4_lname);
                      $("input[name='arr_rev4_email']").val(r4_email);
                  }
              });

              // Reviewer 5
              $("select[name='arr_rev5_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r5_name = opt_array[0].split(', ');
                      var r5_fname = r5_name[1];
                      var r5_lname = r5_name[0];
                      var r5_email = opt_array[1];

                      $("input[name='arr_rev5_fname']").val(r5_fname);
                      $("input[name='arr_rev5_lname']").val(r5_lname);
                      $("input[name='arr_rev5_email']").val(r5_email);
                  }
              });

              // Reviewer 6
              $("select[name='arr_rev6_select']").on('change', function () {
                  var sel = $(this);
                  var opt_text = $('option:selected', sel).text();
                  if (opt_text) {
                      // split on | 
                      var opt_array = opt_text.split(' | ');
                      // now split name
                      var r6_name = opt_array[0].split(', ');
                      var r6_fname = r6_name[1];
                      var r6_lname = r6_name[0];
                      var r6_email = opt_array[1];

                      $("input[name='arr_rev6_fname']").val(r6_fname);
                      $("input[name='arr_rev6_lname']").val(r6_lname);
                      $("input[name='arr_rev6_email']").val(r6_email);
                  }
              });
          });
      </script>

    <?php
  }


  if ($instrument == 'award_status') {
    ?>
      <script type='text/javascript'>
          $(document).ready(function () {

              // Set Benchmark deadlines when Project Start Date is set
              $("input[name='proj_start_date']").on('blur', function () {
                  var st_date = $("input:text[name='proj_start_date']").val();

                  var benchmark_1 = getNewDate(st_date, 141);
                  var benchmark_2 = getNewDate(st_date, 261);
                  var benchmark_3 = getNewDate(st_date, 401);

                  $("input[name='init_dead']").val(benchmark_1).trigger('blur');
                  $("input[name='mid_dead']").val(benchmark_2).trigger('blur');
                  $("input[name='final_dead']").val(benchmark_3).trigger('blur');

              });

              // Set Benchmark deadlines if No-cost Extension is granted
              $("select[name='no_cost_extension']").on('change', function () {
                  var st_date = $("input:text[name='proj_start_date']").val();
                  var nce = $("select[name='no_cost_extension']").val();
                  //alert("sdate: " + st_date);

                  var nce_1 = getNewDate(st_date, 561);
                  var nce_2 = getNewDate(st_date, 766);

                  $("input[name='nocost_mid_dead']").val(nce_1).trigger('blur');
                  $("input[name='nocost_final_dead']").val(nce_2).trigger('blur');

              });

          });

          function getNewDate(contact_date, days) {
              //var date = new Date(contact_date);
              var newdate = new Date(contact_date);
              var numDays = parseInt(days);

              newdate.setDate(newdate.getDate() + numDays);

              var dd = newdate.getDate();
              var mm = newdate.getMonth(); // January is 0
              var y = newdate.getFullYear();
              var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
              var someFormattedDate = months[mm] + ' ' + dd + ', ' + y;

              return someFormattedDate;
          }
      </script>

    <?php
  }

}




/////////////////////////////////////
////  REDCAP_PROJECT_HOME_PAGE  /////
/////////////////////////////////////
if ($hook_event == 'redcap_project_home_page') {
  print "<div class='yellow'><b>NOTE: </b> Email notifications with PDF attachment are in place on this project for the following instruments.<br>
           Intent to Apply, Grant Application, Benchmark</div>";
}

///////////////////////////////////
////  REDCAP_SURVEY_COMPLETE  /////
///////////////////////////////////
if ($hook_event == 'redcap_survey_complete') {
  $inst_array = array('intent_to_apply', 'grant_application', 'benchmark');
  if (in_array($instrument, $inst_array) && isset($event_id) && isset($record)) {
    // get email and name from record
    if ($instrument == 'intent_to_apply') {
      $fields = array('first_name','last_name','degrees','dept_pi','email','email2','role_2','email3','role_3','email4','role_4','email5','role_5','email6','role_6','email7','role_7','email8','role_8','email9','role_9','email10','role_10',);
      // use these arrays to write Co-Pi and Co-Inv values from LOI to Grant Application
      $loi_co_fields = array(array('first_name','last_name','degrees','dept_pi','email'),
                             array('role_2','first_name2','last_name2','degrees_2','inst_2','inst_other_2','dept_2','div_2','div_chief_2','email2'),
                             array('role_3','first_name3','last_name3','degrees_3','inst_3','inst_3b_other','dept_3','div_3','div_chief_3','email3'),
                             array('role_4','first_name4','last_name4','degrees_4','inst_4','inst_4b_other','dept_4','div_4','div_chief_4','email4'),
                             array('role_5','first_name5','last_name5','degrees_5','inst_5','inst_5b_other','dept_5','div_5','div_chief_5','email5'),
                             array('role_6','first_name6','last_name6','degrees_6','inst_6','inst_6b_other','dept_6','div_6','div_chief_6','email6'),
                             array('role_7','first_name7','last_name7','degrees_7','inst_7','inst_7b_other','dept_7','div_7','div_chief_7','email7'),
                             array('role_8','first_name8','last_name8','degrees_8','inst_8','inst_8b_other','dept_8','div_8','div_chief_8','email8'),
                             array('role_9','first_name9','last_name9','degrees_9','inst_9','inst_9b_other','dept_9','div_9','div_chief_9','email9'),
                             array('role_10','first_name10','last_name10','degrees_10','inst_10','inst_10b_other','dept_10','div_10','div_chief_10','email10'),
                             array('role_11','first_name11','last_name11','degrees_11','inst_11','inst_7b_other','dept_11','div_11','div_chief_11','email11'));
      $grant_co_fields = array(array('pi_first','pi_last','pi_degrees','pi_dept','pi_email'),
                               array('coinv1_role','coinv1_first','coinv1_last','coinv1_degrees','coinv1_inst','coinv1_inst_other','coinv1_dept','coinv1_division','coinv1_division_chief','coinv1_email'),
                               array('coinv2_role','coinv2_first','coinv2_last','coinv2_degrees','coinv2_inst','coinv2_inst_other','coinv2_dept','coinv2_division','coinv2_division_chief','coinv2_email'),
                               array('coinv3_role','coinv3_first','coinv3_last','coinv3_degrees','coinv3_inst','coinv3_inst_other','coinv3_dept','coinv3_division','coinv3_division_chief','coinv3_email'),
                               array('coinv4_role','coinv4_first','coinv4_last','coinv4_degrees','coinv4_inst','coinv4_inst_other','coinv4_dept','coinv4_division','coinv4_division_chief','coinv4_email'),
                               array('coinv5_role','coinv5_first','coinv5_last','coinv5_degrees','coinv5_inst','coinv5_inst_other','coinv5_dept','coinv5_division','coinv5_division_chief','coinv5_email'),
                               array('coinv6_role','coinv6_first','coinv6_last','coinv6_degrees','coinv6_inst','coinv6_inst_other','coinv6_dept','coinv6_division','coinv6_division_chief','coinv6_email'),
                               array('coinv7_role','coinv7_first','coinv7_last','coinv7_degrees','coinv7_inst','coinv7_inst_other','coinv7_dept','coinv7_division','coinv7_division_chief','coinv7_email'),
                               array('coinv8_role','coinv8_first','coinv8_last','coinv8_degrees','coinv8_inst','coinv8_inst_other','coinv8_dept','coinv8_division','coinv8_division_chief','coinv8_email'),
                               array('coinv9_role','coinv9_first','coinv9_last','coinv9_degrees','coinv9_inst','coinv9_inst_other','coinv9_dept','coinv9_division','coinv9_division_chief','coinv9_email'),
                               array('coinv10_role','coinv10_first','coinv10_last','coinv10_degrees','coinv10_inst','coinv10_inst_other','coinv10_dept','coinv10_division','coinv10_division_chief','coinv10_email'));
    } else {
      $fields = array('pi_email', 'pi_first','coinv1_email','coinv1_role','coinv2_email','coinv2_role','coinv3_email','coinv3_role','coinv4_email','coinv4_role',
                      'coinv5_email','coinv5_role','coinv6_email','coinv6_role','coinv7_email','coinv7_role','coinv8_email','coinv8_role','coinv9_email','coinv9_role_',);
    }

    $data = json_decode(REDCap:: getData($project_id, 'json', $record, $fields), true);

    // get unique event name and instrument label to be used in confirmation email
    $event = REDCap :: getEventNames(true, false, $event_id);
    $inst_label = REDCap:: getInstrumentNames("$instrument");
    $event_labels = array('application_arm_1' => 'Application', 'benchmark_report_arm_1' => 'Benchmark Report', 'midpoint_report_arm_1' => 'Midpoint Report', 'end_report_arm_1' => 'End Report');

    $pi_email = ($instrument == 'intent_to_apply') ? $data[0]['email'] : $data[0]['pi_email'];
    $name = explode(" ", $data[0]['first_name']);
    $lname = end($name);


    $copi_emails = '';
    $copi_num = 0;
    // determine emails of Co-PIs (not Co-I's) to cc
    if ($instrument == 'intent_to_apply') {
      $filled_roles = 0;
      $loi_fields = array();
      $grant_fields = array();
      for ($i = 2; $i <= 10; $i++) {
        if ($data[0]['role_'. $i] != '') {
          $filled_roles += 1;
          // get co info and add to array to add to grant form
          //print "<br>hi<br>";
          $loi_fields = array_merge($loi_fields, $loi_co_fields[$i - 1]);
          $grant_fields = array_merge($grant_fields,$grant_co_fields[$i - 1]);

          if ($data[0]['role_'. $i] == 1) {
            $copi_emails .= $data[0]['email'. $i] .",";
          }
        }
      }

      $from_loi = array();
      $loi_data = array();
      $to_grant = array();
      $to_grant[$record][$event_id]['investigators'] = $filled_roles;
      // now copy what was provided on the LOI to the Grant Application
      if (count($loi_fields) > 0) {
          // add MCW PI data to $to_grant array
        $to_grant[$record][$event_id]['pi_first'] = "{$data[0]['first_name']}";
        $to_grant[$record][$event_id]['pi_last'] = "{$data[0]['last_name']}";
        $to_grant[$record][$event_id]['pi_degrees'] = "{$data[0]['degrees']}";
        $to_grant[$record][$event_id]['pi_dept'] = "{$data[0]['dept_pi']}";
        $to_grant[$record][$event_id]['pi_email'] = "{$data[0]['email']}";
        // 'first_name','last_name','degrees','dept_pi','email'
        foreach ($loi_fields as $key => $value) {
          $from_loi = json_decode(REDCap:: getData('json', $record, "$value"), true);
          $loi_data[] = $from_loi[0][$value];
        }

        for ($j = 0; $j < count($grant_fields); $j++) {
          $to_grant[$record][$event_id]["{$grant_fields[$j]}"] = "{$loi_data[$j]}";
        }
        // now write to Grant Application form
        if (count($to_grant) > 0) {
          $save = REDCap :: saveData($project_id, 'array', $to_grant, 'normal');
          if (count($save['errors']) > 0) {
              var_dump($save['errors']);
          }
        }
      }
    } else {
      for ($i = 1; $i <= 9; $i++) {
        if ($data[0]['coinv'. $i .'_role'] != '' && $data[0]['coinv'. $i .'_role'] == 1) {
          $copi_emails .= $data[0]['coinv'. $i .'_email'] .",";
        }
      }
    }

    //remove last comma
    $copi_emails = substr($copi_emails,0,-1);

    // email settings
    $from = "rmccoy@mcw.edu";
    $cc = $copi_emails;
    $subject = ($event == "application_arm_1") ? "Completed CTSI $inst_label Form" : "Completed CTSI ".$event_labels[$event]." Form";

    if ($instrument == 'intent_to_apply') {
      $message = "Thank you for submitting your Intent to Apply for the CTSI Traditional Pilot Award.<br><br>Once your Intent to Apply is reviewed and approved, you will be emailed a link for completing the award application.";
    }
    elseif ($instrument == 'grant_application') {
      $message = "Thank you for submitting your application for the CTSI Pilot Translational & Clinical Studies Award.";
    }
    else {
      $message = "Thank you for completing your CTSI Pilot Award  ".$event_labels[$event]." form. Attached is a copy for your records.";
    }

    $message .= '<br><br>
                            <div style="border: 1px solid #ccc; background-color: #ededed; padding: 10px;">
                             <p><strong>Questions?</strong> If you have any questions about this process, please contact Renee McCoy, Program Manager, 
                             CTSI Pilot Award Program. <br><strong>Phone:</strong> 414-955-2524 / <strong>Email:</strong> 
                             <a href="mailto:rmccoy@mcw.edu">rmccoy@mcw.edu</a></p>
                            </div>';


    // set the filename based on the PI, instrument, record and event_id to ensure uniqueness
    $filename = $lname.'_'.$instrument.'_'.date('Ymd')."_".$record."_".$event_id.'.pdf';

    // generate the compact pdf and place it in the temp directory where it will later be deleted automatically
    $pdf_content = REDCap:: getPDF($record, $instrument, $event_id, false,null,true);
    file_put_contents("/tmp/".$filename, $pdf_content);

    // send the email with attached pdf
    // public static function email($to='', $from='', $subject='', $message='', $cc='', $attachmentPath='')
    $sent = REDCap:: email("$pi_email", "$from", "$subject", "$message", "$cc", "/tmp/".$filename);

    // save num of copi's to Grant Application form
    if ($instrument == 'intent_to_apply' && $copi_num > 0) {
        $grant_data = array();
        $grant_data[$record][$event_id]['investigators'] = "$copi_num";
        $save = REDCap :: saveData($project_id, 'array', $grant_data, 'normal');
    }

    // print whether email was successful or not to the bottom of the Survey Acknowledgement page
    if ($sent) {
      unlink("/tmp/".$filename);
      print '<div class="yellow">Confirmation email was successfully sent</div>';
    }
    else {
      print '<div class="red">Confirmation email was NOT successfully sent. Please contact the <a href="mailto:ctsi@mcw.edu">CTSI</a> if you would like a copy.<br>We apologize for this slight inconvenience.</div>';
    }

  }
  else {
    //print "haha!";
  }

  // populate pi_full value. This is intended to help Dan Klaver when pulling data into Tableau
  if ($instrument == 'grant_application') {
    // get relevent data
    $fields = array('pi_first', 'pi_last', 'pi_degrees', $instrument.'_compleete', 'pi_full');
    $data = json_decode(REDCap:: getData('json', $record, $fields, $event_id), true);

    if (!isset($data[0]['pi_full']) || $data[0]['pi_full'] == '') {
      $fullname = $data[0]['pi_first']." ".$data[0]['pi_last'].", ".$data[0]['pi_degrees'];

      $full = array();
      $full[$record][$event_id]['pi_full'] = "$fullname";
      $save = REDCap:: saveData($project_id, 'array', $full);
    }

  }

}


/*********************************
 ****** redcap_save_record  ******
 *********************************/
if ($hook_event == 'redcap_save_record') {

  if ($instrument == 'application_admin') {
    // send each reviewer listed custom link 
    $fields = REDCap:: getFieldNames($instrument);
    $data = json_decode(REDCap:: getData('json', $record, $fields, $event_id), true);
    $proj_data = json_decode(REDCap:: getData('json', $record, array('pi_last', 'study_title','rev_deadline'), 'application_arm_1'), true);
    $email_sent = array();
    $log_text = '';

    // get stored_filename from redcap_edocs_metadata, this is the file uploaded 
    // to this form for this record, retrieved and attached to the email sent to reviewer
    $sql = "select stored_name, doc_name from redcap_edocs_metadata where doc_id = {$data[0]['application_pdf']}";
    $file = db_fetch_assoc(db_query($sql));
    $stored_name = $file['stored_name'];
    $doc_name = $file['doc_name'];

    // check if query failed
    if (empty($stored_name) || $stored_name == '') {
      $log_text .= "File query failed\n";
    }

    // because the file on the server has a cryptic name, 
    // make a copy and rename to original. Then delete the copy at end of script
    $copy = copy(EDOC_PATH.$stored_name, EDOC_PATH.$doc_name);

    // check if copy failed
    if (!$copy) {
      $log_text .= "Copy failed\n";
    }

    // email setup
    $from = 'rmccoy@mcw.edu';
    $subject = 'Link to CTSI Pilot Review Form';

    if ($data[0]['send_rev_emails'] == 1) {
        // loop through all 4 potential reviewers added to form
      for ($i = 1; $i <= 4; $i++) {
        // check if email is not blank, and only send if not already sent
        if ($data[0]['aa_rev'.$i.'_email'] != '' && $data[0]['rev'.$i.'_email_sent'] == '') {

          $link = REDCap:: getSurveyLink($record, 'reviewer_'.$i, $event_id);
          if (empty($link) || $link == null) { $link = "empty"; }
          $to = $data[0]['aa_rev'.$i.'_email'];
          $cc = ($data[0]['aa_rev'.$i.'_cc_email'] != '') ? $data[0]['aa_rev'.$i.'_cc_email'] : '';
          $body = "Dear Dr. {$data[0]['aa_rev'. $i .'_lname']},<br><br>
                             Thank you for agreeing to review the attached application packet for the CTSI Traditional Pilot Award!<br><br>
                             <b>PI:</b> {$proj_data[0]['pi_last']}<br>
                             <b>Title:</b> {$proj_data[0]['study_title']}<br><br>
                             <ul>
                             <li>Please use the link below to access the online review form for this specific proposal.</li>
                             <li>Instructions are included within the online form and are also available to download for your reference.</li>
                             <li>You will have the option to begin the review, save your responses and return later to submit.</li>
                             <li>Please submit your review by <b>{$proj_data[0]['rev_deadline']}</b></li>
                             </ul>
                             <br>If you have any questions or concerns, please contact me as soon as possible.<br><br>
                              <a href=\"$link\">CTSI Pilot Review Survey</a><br><br>
                              If the link above does not work, copy and paste the text below into your web browsers address bar.<br>
                              $link<br><br>
                              Renee McCoy<br>
                              Program Manager<br>
                              CTSI Pilot Award Program<br>
                              Clinical & Translational Science Institute of Southeast WI<br>
                              Medical College of Wisconsin<br>
                              (414) 955-2524 | <a href='mailto:rmccoy@mcw.edu'><u>rmccoy@mcw.edu</u></a>";

          // send email to reviewers
          $sent = REDCap:: email("$to", "$from", "$subject", "$body", "$cc", EDOC_PATH.$doc_name);

          // if sent, set values to update record showing that email was sent and when
          if ($sent) {
            $email_sent[$record][$event_id]['rev'.$i.'_email_sent'] = ($sent) ? '1' : '';
            $email_sent[$record][$event_id]['rev'.$i.'_email_sent_date'] = ($sent) ? date('Y-m-d') : '';
          }

          $log_text = "FullRev $i: {$data[0]['aa_rev'. $i .'_lname']}\n($to)";
          REDCap:: logEvent($log_text, "rec: $record\n i: $i\n event: $event_id\n cc: $cc\n link: $link", null, $record);

        }

        // if necessary, update reviewer count in Reviewer Invitation project (pid967)
        // get current values of num_prop from each record and increment
        $num_prop_val = array();
        $num_prop = json_decode(REDCap:: getData(967, 'json', $data[0]['aa_rev'.$i.'_select'], 'num_prop'), true);
        $num_prop_val[$data[0]['aa_rev'.$i.'_select']][5419]['num_prop'] = ($num_prop[0]['num_prop'] == '') ? 1 : $num_prop[0]['num_prop'] + 1;
        $num_prop_val[$data[0]['aa_rev'.$i.'_select']][5419]['recip_summ'] = 1;
        $save_num_prop = REDCap:: saveData(967, 'array', $num_prop_val, 'overwrite');

      }


      if (count($email_sent) > 0) {
        $email_sent[$record][$event_id]['initial_send_date'] = date("m/d/Y h:i");
        $save = REDCap:: saveData($project_id, 'array', $email_sent, 'normal', 'MDY');
      }
    }
    // delete copied file
    unlink(EDOC_PATH.$doc_name);
  }


  if ($instrument == 'reviewer_request') {
    // send each reviewer listed custom link 
    $fields = REDCap:: getFieldNames($instrument);
    $data = json_decode(REDCap:: getData('json', $record, $fields, $event_id), true);
    $proj_data = json_decode(REDCap:: getData('json', $record, array('pi_last', 'study_title'), 'application_arm_1'), true);
    $email_sent = array();
    $log_text = '';

    // email setup
    $from = 'rmccoy@mcw.edu';
    $cc = '';
    $subject = 'CTSI Pilot Reviewer Request';

    // if Send Reviewer Emails = Yes
    if ($data[0]['arr_send_rev_emails'] == 1) {
      for ($i = 1; $i <= 6; $i++) {
        // if reviewer $i email blank, or not sent, then send
        if ($data[0]['arr_rev'.$i.'_email'] != '' && $data[0]['arr_rev'.$i.'_email_sent'] == '') {
          $link = REDCap:: getSurveyLink($record, 'reviewer_request_'.$i, $event_id);
          $to = $data[0]['arr_rev'.$i.'_email'];
          $body = "Dear Dr. {$data[0]['arr_rev'. $i .'_lname']},<br><br>
                             Thank you for offering to serve as a reviewer for the CTSI Traditional Pilot Award applications. We have one or more proposals that may 
                             align with your area of expertise which we would like to run by you for suitability and preference.<br><br>
                             <b>PI:</b> {$proj_data[0]['pi_last']}<br>
                             <b>Title:</b> {$proj_data[0]['study_title']}<br><br>
                             <ul>
                             <li>Please use the link below to view a <b>project summary</b> and indicate whether you can commit to conducting a full review. 
                             Please also look over the <b>study team members</b> and indicate at the link whether you might have a conflict or relationship to disclose.</li>
                             <li>You may receive more summaries than the number of proposals you have indicated you are able to review - please choose your preferred one(s) by selecting the appropriate responses at each link that you are sent.</li>
                             <li>Depending on the responses we receive, <em>you might not be assigned this proposal even if you indicate you are able to review it</em>.</li>
                             <li>Further details, including review criteria, are provided at the link. <b>Reviews will be due by {$data[0]['rev_deadline']}.</b></li>
                             </ul>
                             <br>Please contact Renee McCoy at 414-955-2524 or <a href='mailto:rmccoy@mcw.edu'><u>rmccoy@mcw.edu</u></a> if you have any questions or concerns.<br><br>
                             <b>Your prompt response is greatly appreciated! Thank you!</b><br><br>
                             <a href=\"$link\">CTSI Pilot Review Request Survey</a><br><br>
                             If the link above does not work, copy and paste the text below into your web browsers address bar.<br>
                             $link<br><br>
                             Renee McCoy<br>
                             Program Manager<br>
                             CTSI Pilot Award Program<br>
                             Clinical & Translational Science Institute of Southeast WI<br>
                             Medical College of Wisconsin<br>
                             (414) 955-2524";

          // send email to reviewers
          $sent = REDCap:: email("$to", "$from", "$subject", "$body", "$cc");

          $email_sent[$record][$event_id]['arr_rev'.$i.'_email_sent'] = ($sent) ? '1' : '';
          $email_sent[$record][$event_id]['arr_rev'.$i.'_email_sent_date'] = ($sent) ? date('Y-m-d') : '';
          $log_text .= "Review request survey sent: {$data[0]['arr_rev'. $i .'_lname']} ($to)\n";

        }
      }

      if (count($email_sent) > 0) {
        $save = REDCap:: saveData($project_id, 'array', $email_sent,'overwrite');
        REDCap:: logEvent($log_text, null, null, $record);
      }
    }
  }


  if ($instrument == 'review_results_admin') {
      // get reviewer data from Application Admin form
    $rev_fields = REDCap :: getFieldNames('application_admin');
    $rev_data = json_decode(REDCap :: getData('json', $record, $rev_fields, $event_id), true);

    // get data submitted on form
    $form_fields = REDCap :: getFieldNames($instrument);
    $form_data = json_decode(REDCap :: getData('json', $record, $form_fields, $event_id), true);

    $proj_fields = array('pi_last', 'study_title');
    $proj_data = json_decode(REDCap:: getData('json', $record, $proj_fields, 'application_arm_1'), true);

    $imp_score = array(1 => "1: Exceptional","2: Outstanding","3: Excellent","4: Very Good","5: Good","6: Satisfactory","7: Fair","8: Marginal","9: Poor");

    if ($form_data[0]['send_rev_result_email'] == '1') {
        // get data from Reviewer surveys to add to emails going out
      $reviewer_scores = array('r1_imp_score','r1_imp_just','r2_imp_score','r2_imp_just','r3_imp_score','r3_imp_just','r4_imp_score','r4_imp_just');
      $reviewer_scores = json_decode(REDCap :: getData('json', $record, $survey_fields, $event_id), true);

      $rev1_scores = "<u><b>Reviewer #1</b></u><br><table style='width:100%;border:1px solid black;'><tr><td style='width:15%'><b>Overall Impact Score:</b></td><td>{$imp_score[$reviewer_scores[0]['r1_imp_score']]}</td></tr>
                      <tr><td style='width:15%'><b>Justification:</b></td><td>{$reviewer_scores[0]['r1_imp_just']}</td></tr></table><br>";

      $rev2_scores = "<u><b>Reviewer #2</b></u><br><table style='width:100%;border:1px solid black;'><tr><td style='width:15%'><b>Overall Impact Score:</b></td><td>{$imp_score[$reviewer_scores[0]['r2_imp_score']]}</td></tr>
                      <tr><td style='width:15%'><b>Justification:</b></td><td>{$reviewer_scores[0]['r2_imp_just']}</td></tr></table><br>";

      $rev3_scores = "<u><b>Reviewer #3</b></u><br><table style='width:100%;border:1px solid black;'><tr><td style='width:15%'><b>Overall Impact Score:</b></td></td><td>{$imp_score[$reviewer_scores[0]['r3_imp_score']]}</td></tr>
                      <tr><td style='width:15%'><b>Justification:</b></td><td>{$reviewer_scores[0]['r3_imp_just']}</td></tr></table><br>";

      $rev4_scores = "<u><b>Reviewer #4</b></u><br>(where applicable)<br><table style='width:100%;border:1px solid black;'><tr><td style='width:15%'><b>Overall Impact Score:</b></td><td>{$imp_score[$reviewer_scores[0]['r4_imp_score']]}</td></tr>
                      <tr><td style='width:15%'><b>Justification:</b></td><td>{$reviewer_scores[0]['r4_imp_just']}</td></tr></table><br>";


      // set up email
      $from = 'rmccoy@mcw.edu';
      $cc = '';
      $subject = 'CTSI Pilot Review Results';
      $msg1 = "Dear Colleague:<br><br>
               Thank you for submitting your review for our  2019 cycle of CTSI Pilot Award proposals. We are very grateful for your service!<br><br>
               The Overall Impact Scores and Justifications for the proposal you have reviewed are provided anonymously in the table below as an enhanced feature of our review process.<br><br>
               You have the option of returning to your original review form to make revisions if you choose by using the link at the bottom of this email. Please note that this option will only be available through 
               <b>Wednesday, October 10</b>, at which time all reviews will be considered final and you will no longer be able to access your review form.<br><br>
               <b>PI:</b> {$proj_data[0]['pi_last']}<br>
               <b>Title:</b> {$proj_data[0]['study_title']}<br><br>";

      $msg2 = "If you have any questions, please contact Renee McCoy at rmccoy@mcw.edu or 414-955-2524.<br><br>Thank you again!<br><br>
               Renee McCoy<br>
               Program Manager<br>
               CTSI Pilot Award Program<br>
               Clinical & Translational Science Institute of Southeast WI<br>
               Medical College of Wisconsin<br>
               (414) 955-2524";

        // check if email already sent
        for ($i = 1; $i <= 4; $i++) {

            if ($form_data[0]['rev'.$i.'_res_email_sent'] == '') {
                $survey_link = "Your original Review link<br>". REDCap :: getSurveyLink($record,"reviewer_". $i, $event_id) ."<br><br>";
                $to = $rev_data[0]['aa_rev'. $i .'_email'];
                $body = $msg1 . "<b><u>In the table below you are Reviewer #$i</u></b><br><br>" . $rev1_scores . $rev2_scores . $rev3_scores . $rev4_scores . $survey_link . $msg2;
                $sent = REDCap :: email("$to", "$from", "$subject", "$body", "$cc");

                if ($sent) {
                    $email_sent = array();
                    $email_sent[$record][$event_id]['rev'.$i.'_res_email_sent'] = '1';
                    $email_sent[$record][$event_id]['rev'.$i.'_res_email_sent_date'] = date('Y-m-d');

                    $save = REDCap:: saveData($project_id, 'array', $email_sent);
                }

            }

        }

    }

  }


  if ($instrument == 'semifinalists') {
      // get form data
    $form_fields = REDCap :: getFieldNames($instrument);
    $form_data = json_decode(REDCap :: getData('json',$record,$form_fields),true);

    if ($form_data[0]['sf_email_sent'] != '1' && $form_data[0]['semifinalist'] != '') {
        // get PI and co-PI data from Grant Application form
      $pi_fields = array('study_title','pi_last','pi_email','coinv1_last','coinv1_role','coinv1_email', 'coinv2_last','coinv2_role','coinv2_email','coinv3_last','coinv3_role','coinv3_email','coinv4_last','coinv4_role','coinv4_email',
                         'coinv5_last','coinv5_role','coinv5_email','coinv6_last','coinv6_role','coinv6_email','coinv7_last','coinv7_role','coinv7_email','coinv8_last','coinv8_role','coinv8_email',
                         'coinv9_last','coinv9_role','coinv9_email','coinv10_last','coinv10_role','coinv10_email');
      $pi_data = json_decode(REDCap :: getData('json',$record,$pi_fields),true);

      $pi_email = $pi_data[0]['pi_email'];
      $pi_last = $pi_data[0]['pi_last'];
      $docs = "Dr. $pi_last";
      $to = "$pi_email";
      $cc = 'acalhoun@mcw.edu,';
      $title = $pi_data[0]['study_title'];
      $irb_deadline = $form_data[0]['irb_sub_deadline'];

      // parse through PI Data to get all the co-PIs who will be added to the $to list
      for ($i = 1; $i <= 10; $i++) {
          // if coinv _role == 'Co-PI', add email
          if ($pi_data[0]['coinv'. $i .'_role'] == 1) {
              $to .= ",". $pi_data[0]['coinv'. $i .'_email'];
              $docs .= ", Dr. {$pi_data[0]['coinv'. $i .'_last']}";
          }

      }

      // parse through the form data to get all who will be cc'd on the email
      for ($i = 1; $i <= 10; $i++) {
          if ($form_data[0]['sf_email_cc_'. $i] != '') {
              $cc .= "{$form_data[0]['sf_email_cc_'. $i]},";
          }
      }

      // remove last comma
      $cc = substr($cc, 0, -1);

      // setup email
      $from = 'rmccoy@mcw.edu';
      $subject = '2019 CTSI Traditional Pilot Award';

      $header = "<img src='https://redcap.mcw.edu/ctsi_logo_newer.jpg' width='400px' alt='' /><br><br>";
      $greeting = "Dear $docs,<br><br>";
      $salutation = "Best regards,<br><br>
                     Renee B. McCoy<br>
                     Program Manager<br>
                     CTSI Pilot Award Program<br>
                     Clinical & Translational Science Institute<br>
                     Medical College of Wisconsin<br>
                     <a href='mailto:rmccoy@mcw.edu'>rmccoy@mcw.edu</a> | (414) 955-2524<br><br>
                     UL1 TR001436, TL1 TR001437, KL2 TR001438<br>
                     Cite the NIH CTSA grant and acknowledge support";

      if ($form_data[0]['semifinalist'] == '1') {
          $body = "I am pleased to notify you that your 2019 CTSI Traditional Pilot Award application <b><i>$title</i></b> has completed the review process and has been recommended for 
                   funding for $50,000 with a project start date of April 1, 2019.<br><br>
                   <font color='red'><b><u>RESPONSE NEEDED:</u></font> Please reply to this email by <u>November 26, 2018</u> to confirm you intend to accept this recommendation and will complete the pre-award requirements.</b><br><br>
                   Additional documentation is needed before a commitment for funding can be made and an award letter will be sent after you have met all pre-award requirements, including those outlined below:<br>
                   <ol><li>PI and Co-PI attendance at a mandatory meeting to discuss your specific pre-award and post-award requirements (you will be contacted by CTSI Administrative Coordinator, Amy Calhoun, for scheduling);</li>
                   <li>Institutional and regulatory approvals, including required training, for: human subjects research; animal research; safety; new drugs/devices; etc., as applicable.</li>
                   <ul><li><b>Please submit your IRB protocol by $irb_deadline,</b> if applicable, in order to allow the IRB sufficient time to review and process the request. Subsequent approvals, such as NCATS Prior NIH Approval, 
                   <u>will hinge upon IRB approval</u>, so expeditious submission and diligent follow up is extremely important to avoid delays in receiving your award letter.</li>
                   <li>If your study will involve any Froedtert Hospital resources, including medical records, you must apply for <a href='https://www.froedtert.com/research/ocricc'>OCRICC approval</a>; please add “CTSI PILOT” 
                   in the title of your OCRICC application which will help to expedite the approval process.</li></ul>
                   <li>Timely submission of required documentation for NCATS Prior NIH Approval (details and full instructions will be sent to you in December);</li>
                   <li>2019 CTSI Traditional Pilot Awards are funded via a parent award from the Medical College of Wisconsin’s Advancing a Healthier Wisconsin Research and Education Program (AHW REP) and are therefore 
                   subject to requirements as outlined in the, <a href='https://ahwendowment.org/AHW1/Partner-Portal/REP-Documents/AHWREAwardAdministrationManual_Sept2018.pdf'>AHW Research and Education Award Administration Manual</a> (PDF). 
                   In particular, verification of Non-supplanting of funds is needed before funding will be approved.</li></ol>
                   Instructions and guidance for fulfilling these requirements will be discussed during the mandatory meeting and sent via email over the next few months prior to the project start date. Your timely cooperation 
                   will help to ensure that your pilot research can begin as soon as possible. If you have any questions, please do not hesitate to contact me. My apologies if I inadvertently did not include a member of your team, 
                   please forward this communication as needed.<br><br>
                   I look forward to working with you as you embark on your pilot study!<br><br>";

      } elseif ($form_data[0]['semifinalist'] == '0') {
          $body = "Thank you for submitting your application for the 2019 CTSI Pilot Translational and Clinical Studies Program Traditional Pilot Awards. Unfortunately, your proposal was not among those selected for funding. 
                   This had been a highly competitive cycle with 34 proposals submitted of which 12 projects will receive recommendations for funding. The review process involved individual reviewers providing scores and 
                   feedback followed by discussion within two separate oversight committees.<br><br>
                   Applications were scored based on scientific merit, translational focus, and other criteria included in the 2019 CTSI Pilot Award RFA. Attached to this message are comments from the reviewers about your proposal.<br><br>
                   Thank you again for applying to our Traditional Pilot Award and we hope that you will consider applying for this, and our other translational, collaborative funding opportunities in the future.<br><br>";

          // setup pdf file of Reviewer Feedback for Applicants for attaching to email
          // set the filename based on the PI, instrument, record and event_id to ensure uniqueness
          $filename = $pi_last.'_2019CTSI_PilotFeedback_'.date('Ymd').'.pdf';

          // generate the pdf and place it in the temp directory where it will later be deleted automatically
          $pdf_content = REDCap:: getPDF($record, "reviewer_feedback_for_applicants", $event_id, false,null,true);
          file_put_contents("/tmp/".$filename, $pdf_content);

      }
      // send email
      $msg = $header . $greeting . $body . $salutation;
      $sent = ($form_data[0]['semifinalist'] == '1') ? REDCap :: email("$to","$from","$subject","$msg","$cc") : REDCap :: email("$to","$from","$subject","$msg","$cc","/tmp/".$filename);

      if ($sent) {
          // update record
        $email_sent = array();
        $email_sent[$record][$event_id]['sf_email_sent'] = '1';
        $email_sent[$record][$event_id]['sf_email_sent_date'] = date('Y-m-d');

        $save = REDCap:: saveData($project_id, 'array', $email_sent);


      } else {
        // update record
        $email_sent = array();
        $email_sent[$record][$event_id]['sf_email_sent'] = '0';

        $save = REDCap :: saveData($project_id, 'array', $email_sent);
      }

    }

  }

}


?>